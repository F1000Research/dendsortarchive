# README #


### What is this repository for? ###

This code has been forked as an archival record for the F1000Research publication dendsort: modular leaf ordering methods for dendro-gram representations in R (doi:  10.12688/f1000research.4784.1).

Therefore this branch is not active. For the latest version of the code please see the master branch at https://bitbucket.org/biovizleuven/dendsort/wiki/Home.